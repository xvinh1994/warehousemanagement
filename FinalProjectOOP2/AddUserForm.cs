﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BussinessLogic;

namespace FinalProjectOOP2
{
    public partial class AddUserForm : Form
    {
        public AddUserForm()
        {
            InitializeComponent();
        }

        private void buttonMore_Click(object sender, EventArgs e)
        {
            if (TestInput())
            {
                DTOEmployee newUser = UserToAdd();

                BUSEmployee busEmp = new BUSEmployee();
                busEmp.AddNewUser(newUser);

                ClearText();
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (TestInput())
            {
                DTOEmployee newUser = UserToAdd();

                BUSEmployee busEmp = new BUSEmployee();
                busEmp.AddNewUser(newUser);

                this.Close();
            }
        }

        private bool TestInput()
        {
            if (txtBoxEID.Text == "")
            {
                MessageBox.Show("Please input EID.");
                txtBoxEID.Focus();
                return false;
            }

            if (txtBosPass.Text == "")
            {
                MessageBox.Show("Please input password.");
                txtBosPass.Focus();
                return false;
            }

            if (txtBoxName.Text == "")
            {
                MessageBox.Show("Please input name.");
                txtBoxName.Focus();
                return false;
            }

            if (txtBoxUsername.Text == "")
            {
                MessageBox.Show("Please input username.");
                txtBoxUsername.Focus();
                return false;
            }

            return true;
        }

        private void ClearText()
        {
            txtBoxUsername.Text = "";
            txtBoxName.Text = "";
            txtBoxEID.Text = "";
            txtBosPass.Text = "";
        }

        private DTOEmployee UserToAdd()
        {
            DTOEmployee result = new DTOEmployee();

            result.EID = txtBoxEID.Text;
            result.Name = txtBoxName.Text;
            result.Password = txtBosPass.Text;
            result.Supplier_id = comboBoxSID.Text;
            result.Username = txtBoxUsername.Text;

            return result;
        }

        private void AddUserForm_Load(object sender, EventArgs e)
        {
            comboBoxSID.DataSource = DatabaseHelper.GetSuppliers();
            comboBoxSID.DisplayMember = "Supplier_id";
        }
    }
}
