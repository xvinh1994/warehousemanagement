﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogic;
using DTO;

namespace FinalProjectOOP2
{
    public partial class ImportForm : Form
    {
        List<DTO.DTOProduct> dsHang = null;
            
        public ImportForm()
        {
            InitializeComponent();
        }

        private void ImportForm_Load(object sender, EventArgs e)
        {
            if (DatabaseHelper.isAdmin) dsHang = DatabaseHelper.GetProductList().ToList();
            else dsHang = DatabaseHelper.GetProductList().ToList().FindAll(x => x.Supplier_id == DatabaseHelper.GetLoggedInUser().Supplier_id);
            
            DTOEmployee user = DatabaseHelper.GetLoggedInUser();

            textBoxEID.Text = user.EID;
            textBoxSID.Text = user.Supplier_id;
            textBoxIBID.Text = DatabaseHelper.GetInBillList().Count.ToString();

            if (dsHang.Count > 0)
            {
                comboBoxName.DataSource = dsHang;
                comboBoxName.DisplayMember = "Name";
                comboBoxName.Text = comboBoxName.Items[0].ToString();

                textBoxDes.Text = dsHang[0].Description;
                textBoxPID.Text = dsHang[0].PID;
                textBoxStorage.Text = dsHang[0].Quantity.ToString();
            }
        }

        private void comboBoxName_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxDes.Text = dsHang[comboBoxName.SelectedIndex].Description;
            textBoxPID.Text = dsHang[comboBoxName.SelectedIndex].PID;
            textBoxStorage.Text = dsHang[comboBoxName.SelectedIndex].Quantity.ToString();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int quantity = 0;

            if (int.TryParse(textBoxNumIn.Text, out quantity))
            {
                DTOProduct oldProduct = dsHang[comboBoxName.SelectedIndex];
                DTOProduct newProduct = dsHang[comboBoxName.SelectedIndex];
                newProduct.Quantity += quantity;
                dsHang[comboBoxName.SelectedIndex] = newProduct;

                BUSProduct busProduct = new BUSProduct();
                busProduct.UpdateProduct(oldProduct, newProduct);

                BUSInProduct busInProduct = new BUSInProduct();
                DTOInProduct inProduct = new DTOInProduct();
                inProduct.IBID = textBoxIBID.Text;
                inProduct.PID = textBoxPID.Text;
                inProduct.Quantity = quantity;
                busInProduct.AddNewInBill(inProduct);

                BUSInBill busInBill = new BUSInBill();
                DTOInBill inBill = new DTOInBill();
                inBill.Date = DateTime.Now;
                inBill.EID = textBoxEID.Text;
                inBill.IBID = textBoxIBID.Text;
                inBill.Reason = textBoxReason.Text;
                inBill.Supplier_id = textBoxSID.Text;
                busInBill.AddNewInBill(inBill);
            }
            else
            {
                MessageBox.Show("Please input correct number!!!");
                textBoxNumIn.Focus();
            }
        }
    }
}
