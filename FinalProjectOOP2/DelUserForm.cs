﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BussinessLogic;

namespace FinalProjectOOP2
{
    public partial class DelUserForm : Form
    {
        public DelUserForm()
        {
            InitializeComponent();
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            DTOEmployee selected = DatabaseHelper.GetUserList().ToList().Find(x => x.EID == comboBoxUser.SelectedValue.ToString());

            BUSEmployee busEmp = new BUSEmployee();
            busEmp.DeleteUser(selected);
        }

        private void DelUserForm_Load(object sender, EventArgs e)
        {
            comboBoxUser.DataSource = DatabaseHelper.GetUserList();
            comboBoxUser.DisplayMember = "EID";
            comboBoxUser.ValueMember = "EID";
        }

        private void comboBoxUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            DTOEmployee selected = DatabaseHelper.GetUserList().ToList().Find(x => x.EID == comboBoxUser.SelectedValue.ToString());
            labelUser.Text = selected.Username;
        }
    }
}
