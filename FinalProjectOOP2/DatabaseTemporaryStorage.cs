﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.ComponentModel;

namespace FinalProjectOOP2
{
    class DatabaseTemporaryStorage
    {
        public static BindingList<DTOEmployee> dsNVien = new BindingList<DTOEmployee>();
        public static BindingList<DTOInBill> dsHoaDonNhap = new BindingList<DTOInBill>();
        public static BindingList<DTOInProduct> dsHangNhap = new BindingList<DTOInProduct>();
        public static BindingList<DTOOutBill> dsHoaDonXuat = new BindingList<DTOOutBill>();
        public static BindingList<DTOOutProduct> dsHangXuat = new BindingList<DTOOutProduct>();
        public static BindingList<DTOProduct> dsSanPham = new BindingList<DTOProduct>();
        public static BindingList<DTOSupplier> dsNhaCungCap = new BindingList<DTOSupplier>();
    }
}
