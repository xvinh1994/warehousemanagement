﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BussinessLogic;

namespace FinalProjectOOP2
{
    public partial class GrantForm : Form
    {
        public GrantForm()
        {
            InitializeComponent();
        }

        private void GrantForm_Load(object sender, EventArgs e)
        {
            BindingList<DTOEmployee> dsNvien = DatabaseHelper.GetUserList();
            dsNvien.Remove(dsNvien.ToList().Find(x => x == DatabaseHelper.GetLoggedInUser()));

            for (int i = 0; i < dsNvien.Count; i++)
            {
                UCGrant newUC = new UCGrant(dsNvien[i]);
                newUC.Location = new Point(panelData.Location.X, panelData.Location.Y + i * 23);
                panelData.Controls.Add(newUC);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < panelData.Controls.Count; i++)
            {
                UCGrant uc = panelData.Controls[i] as UCGrant;

                if (uc.isChanged)
                {
                    DTOEmployee nvCu = uc.GetEmployee();
                    DTOEmployee nvMoi = uc.GetEmployee();
                    nvMoi.IsAdmin = !nvCu.IsAdmin;

                    BUSEmployee busEmp = new BUSEmployee();
                    busEmp.UpdateEmployee(nvCu, nvMoi);
                }
            }

            this.Close();
        }
    }
}
