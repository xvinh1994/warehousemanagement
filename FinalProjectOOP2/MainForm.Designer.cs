﻿namespace FinalProjectOOP2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnExit = new DevExpress.XtraBars.BarButtonItem();
            this.btnInformation = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.btnChangePassword = new DevExpress.XtraBars.BarButtonItem();
            this.btnBackup = new DevExpress.XtraBars.BarButtonItem();
            this.btnRestore = new DevExpress.XtraBars.BarButtonItem();
            this.btnPartnerEmp = new DevExpress.XtraBars.BarButtonItem();
            this.btnSuppiler = new DevExpress.XtraBars.BarButtonItem();
            this.btnProductGroup = new DevExpress.XtraBars.BarButtonItem();
            this.btnProduct = new DevExpress.XtraBars.BarButtonItem();
            this.btnUniEmp = new DevExpress.XtraBars.BarButtonItem();
            this.btnConnect = new DevExpress.XtraBars.BarButtonItem();
            this.btnGrant = new DevExpress.XtraBars.BarButtonItem();
            this.btnImport = new DevExpress.XtraBars.BarButtonItem();
            this.btnExport = new DevExpress.XtraBars.BarButtonItem();
            this.btnStock = new DevExpress.XtraBars.BarButtonItem();
            this.btnImportBill = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportBill = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.btnLogin = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddUser = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteUser = new DevExpress.XtraBars.BarButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteRow = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddRow = new DevExpress.XtraBars.BarButtonItem();
            this.barDateTime = new DevExpress.XtraBars.BarStaticItem();
            this.barStatus = new DevExpress.XtraBars.BarEditItem();
            this.barStatusLoading = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.btnDone = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddRowSymbol = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteRowSymbol = new DevExpress.XtraBars.BarButtonItem();
            this.btnImportItem = new DevExpress.XtraBars.BarButtonItem();
            this.btnExportItem = new DevExpress.XtraBars.BarButtonItem();
            this.rbpSystemMng = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rbpManage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rbpWarehouse = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemProgressBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemMarqueeProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.repositoryItemMarqueeProgressBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.panel = new DevExpress.XtraEditors.PanelControl();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barStatusLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.btnExit,
            this.btnInformation,
            this.skinRibbonGalleryBarItem1,
            this.btnChangePassword,
            this.btnBackup,
            this.btnRestore,
            this.btnPartnerEmp,
            this.btnSuppiler,
            this.btnProductGroup,
            this.btnProduct,
            this.btnUniEmp,
            this.btnConnect,
            this.btnGrant,
            this.btnImport,
            this.btnExport,
            this.btnStock,
            this.btnImportBill,
            this.btnExportBill,
            this.ribbonGalleryBarItem1,
            this.btnLogin,
            this.btnAddUser,
            this.btnDeleteUser,
            this.btnEdit,
            this.btnDeleteRow,
            this.btnAddRow,
            this.barDateTime,
            this.barStatus,
            this.btnDone,
            this.btnAddRowSymbol,
            this.btnDeleteRowSymbol,
            this.btnImportItem,
            this.btnExportItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonControl1.MaxItemId = 68;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rbpSystemMng,
            this.rbpManage,
            this.rbpWarehouse});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemProgressBar2,
            this.repositoryItemMarqueeProgressBar1,
            this.repositoryItemMarqueeProgressBar2,
            this.barStatusLoading});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl1.Size = new System.Drawing.Size(883, 144);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            // 
            // btnExit
            // 
            this.btnExit.Caption = "Exit";
            this.btnExit.Glyph = ((System.Drawing.Image)(resources.GetObject("btnExit.Glyph")));
            this.btnExit.Id = 3;
            this.btnExit.Name = "btnExit";
            this.btnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExit_ItemClick);
            // 
            // btnInformation
            // 
            this.btnInformation.Caption = "Information";
            this.btnInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("btnInformation.Glyph")));
            this.btnInformation.Id = 4;
            this.btnInformation.Name = "btnInformation";
            this.btnInformation.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInformation_ItemClick);
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 5;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Caption = "Change Password";
            this.btnChangePassword.Id = 6;
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnChangePassword_ItemClick);
            // 
            // btnBackup
            // 
            this.btnBackup.Caption = "Backup";
            this.btnBackup.Glyph = ((System.Drawing.Image)(resources.GetObject("btnBackup.Glyph")));
            this.btnBackup.Id = 7;
            this.btnBackup.Name = "btnBackup";
            // 
            // btnRestore
            // 
            this.btnRestore.Caption = "Restore";
            this.btnRestore.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRestore.Glyph")));
            this.btnRestore.Id = 8;
            this.btnRestore.Name = "btnRestore";
            // 
            // btnPartnerEmp
            // 
            this.btnPartnerEmp.Caption = "Employee";
            this.btnPartnerEmp.Id = 9;
            this.btnPartnerEmp.Name = "btnPartnerEmp";
            this.btnPartnerEmp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPartnerEmp_ItemClick);
            // 
            // btnSuppiler
            // 
            this.btnSuppiler.Caption = "Supplier";
            this.btnSuppiler.Id = 10;
            this.btnSuppiler.Name = "btnSuppiler";
            this.btnSuppiler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSuppiler_ItemClick);
            // 
            // btnProductGroup
            // 
            this.btnProductGroup.Id = 25;
            this.btnProductGroup.Name = "btnProductGroup";
            // 
            // btnProduct
            // 
            this.btnProduct.Id = 26;
            this.btnProduct.Name = "btnProduct";
            // 
            // btnUniEmp
            // 
            this.btnUniEmp.Caption = "Employee";
            this.btnUniEmp.Id = 13;
            this.btnUniEmp.Name = "btnUniEmp";
            this.btnUniEmp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUniEmp_ItemClick);
            // 
            // btnConnect
            // 
            this.btnConnect.Caption = "Connect";
            this.btnConnect.Glyph = global::FinalProjectOOP2.Properties.Resources.deletedatasource_16x16;
            this.btnConnect.Id = 14;
            this.btnConnect.LargeGlyph = global::FinalProjectOOP2.Properties.Resources.addnewdatasource_32x32;
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btnConnect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConnect_ItemClick);
            // 
            // btnGrant
            // 
            this.btnGrant.Caption = "Grant";
            this.btnGrant.Enabled = false;
            this.btnGrant.Glyph = ((System.Drawing.Image)(resources.GetObject("btnGrant.Glyph")));
            this.btnGrant.Id = 15;
            this.btnGrant.Name = "btnGrant";
            this.btnGrant.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGrant_ItemClick);
            // 
            // btnImport
            // 
            this.btnImport.Caption = "Import";
            this.btnImport.Id = 16;
            this.btnImport.Name = "btnImport";
            this.btnImport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnImport_ItemClick);
            // 
            // btnExport
            // 
            this.btnExport.Caption = "Export";
            this.btnExport.Enabled = false;
            this.btnExport.Id = 17;
            this.btnExport.Name = "btnExport";
            this.btnExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExport_ItemClick);
            // 
            // btnStock
            // 
            this.btnStock.Caption = "Stock";
            this.btnStock.Id = 18;
            this.btnStock.Name = "btnStock";
            this.btnStock.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnStock_ItemClick);
            // 
            // btnImportBill
            // 
            this.btnImportBill.Caption = "Import Bill";
            this.btnImportBill.Id = 19;
            this.btnImportBill.Name = "btnImportBill";
            this.btnImportBill.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnImportBill_ItemClick);
            // 
            // btnExportBill
            // 
            this.btnExportBill.Caption = "Export Bill";
            this.btnExportBill.Id = 20;
            this.btnExportBill.Name = "btnExportBill";
            this.btnExportBill.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportBill_ItemClick);
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 21;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // btnLogin
            // 
            this.btnLogin.Caption = "Login";
            this.btnLogin.Enabled = false;
            this.btnLogin.Glyph = global::FinalProjectOOP2.Properties.Resources.assignto_16x16;
            this.btnLogin.Id = 22;
            this.btnLogin.LargeGlyph = global::FinalProjectOOP2.Properties.Resources.assignto_32x32;
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLogin_ItemClick);
            // 
            // btnAddUser
            // 
            this.btnAddUser.Caption = "Add User";
            this.btnAddUser.Enabled = false;
            this.btnAddUser.Id = 23;
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddUser_ItemClick);
            // 
            // btnDeleteUser
            // 
            this.btnDeleteUser.Caption = "Delete User";
            this.btnDeleteUser.Enabled = false;
            this.btnDeleteUser.Id = 24;
            this.btnDeleteUser.Name = "btnDeleteUser";
            this.btnDeleteUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteUser_ItemClick);
            // 
            // btnEdit
            // 
            this.btnEdit.Caption = "Edit";
            this.btnEdit.Enabled = false;
            this.btnEdit.Glyph = global::FinalProjectOOP2.Properties.Resources.editdatasource_16x16;
            this.btnEdit.Id = 27;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_ItemClick);
            // 
            // btnDeleteRow
            // 
            this.btnDeleteRow.Caption = "Delete Row";
            this.btnDeleteRow.Enabled = false;
            this.btnDeleteRow.Glyph = global::FinalProjectOOP2.Properties.Resources.delete_16x16;
            this.btnDeleteRow.Id = 28;
            this.btnDeleteRow.Name = "btnDeleteRow";
            this.btnDeleteRow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteRow_ItemClick);
            // 
            // btnAddRow
            // 
            this.btnAddRow.Caption = "Add Row";
            this.btnAddRow.Enabled = false;
            this.btnAddRow.Glyph = global::FinalProjectOOP2.Properties.Resources.addnewdatasource_16x16;
            this.btnAddRow.Id = 29;
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddRow_ItemClick);
            // 
            // barDateTime
            // 
            this.barDateTime.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barDateTime.Caption = "Date Time";
            this.barDateTime.Id = 52;
            this.barDateTime.Name = "barDateTime";
            this.barDateTime.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStatus
            // 
            this.barStatus.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStatus.CanOpenEdit = false;
            this.barStatus.Caption = "Status";
            this.barStatus.Edit = this.barStatusLoading;
            this.barStatus.Id = 62;
            this.barStatus.Name = "barStatus";
            // 
            // barStatusLoading
            // 
            this.barStatusLoading.MarqueeAnimationSpeed = 500;
            this.barStatusLoading.Name = "barStatusLoading";
            this.barStatusLoading.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.barStatusLoading.ReadOnly = true;
            this.barStatusLoading.Stopped = true;
            // 
            // btnDone
            // 
            this.btnDone.Caption = "Done!!!";
            this.btnDone.Enabled = false;
            this.btnDone.Id = 63;
            this.btnDone.Name = "btnDone";
            this.btnDone.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDone_ItemClick);
            // 
            // btnAddRowSymbol
            // 
            this.btnAddRowSymbol.Glyph = global::FinalProjectOOP2.Properties.Resources.AddButton;
            this.btnAddRowSymbol.Id = 64;
            this.btnAddRowSymbol.Name = "btnAddRowSymbol";
            this.btnAddRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnAddRowSymbol.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddRowSymbol_ItemClick);
            // 
            // btnDeleteRowSymbol
            // 
            this.btnDeleteRowSymbol.Glyph = global::FinalProjectOOP2.Properties.Resources.delete_32x32;
            this.btnDeleteRowSymbol.Id = 65;
            this.btnDeleteRowSymbol.Name = "btnDeleteRowSymbol";
            this.btnDeleteRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnDeleteRowSymbol.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteRowSymbol_ItemClick);
            // 
            // btnImportItem
            // 
            this.btnImportItem.Caption = "Import Product";
            this.btnImportItem.Enabled = false;
            this.btnImportItem.Glyph = global::FinalProjectOOP2.Properties.Resources.import_icon;
            this.btnImportItem.Id = 66;
            this.btnImportItem.Name = "btnImportItem";
            this.btnImportItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnImportItem_ItemClick);
            // 
            // btnExportItem
            // 
            this.btnExportItem.Caption = "Export Product";
            this.btnExportItem.Enabled = false;
            this.btnExportItem.Glyph = global::FinalProjectOOP2.Properties.Resources.export_icon;
            this.btnExportItem.Id = 67;
            this.btnExportItem.Name = "btnExportItem";
            this.btnExportItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExportItem_ItemClick);
            // 
            // rbpSystemMng
            // 
            this.rbpSystemMng.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4});
            this.rbpSystemMng.Name = "rbpSystemMng";
            this.rbpSystemMng.Text = "System Manager";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnConnect);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnLogin);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnInformation);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnExit);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "System";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnGrant);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnChangePassword);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnAddUser);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnDeleteUser);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Security";
            this.ribbonPageGroup3.Visible = false;
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btnBackup);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnRestore);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Database";
            this.ribbonPageGroup4.Visible = false;
            // 
            // rbpManage
            // 
            this.rbpManage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5,
            this.ribbonPageGroup9,
            this.ribbonPageGroup10});
            this.rbpManage.Name = "rbpManage";
            this.rbpManage.Text = "Manage";
            this.rbpManage.Visible = false;
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.btnPartnerEmp);
            this.ribbonPageGroup5.ItemLinks.Add(this.btnSuppiler);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Partner";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.btnImport);
            this.ribbonPageGroup9.ItemLinks.Add(this.btnExport);
            this.ribbonPageGroup9.ItemLinks.Add(this.btnStock);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "Product";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.btnUniEmp);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "Unilever";
            this.ribbonPageGroup10.Visible = false;
            // 
            // rbpWarehouse
            // 
            this.rbpWarehouse.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7});
            this.rbpWarehouse.Name = "rbpWarehouse";
            this.rbpWarehouse.Text = "Warehouse";
            this.rbpWarehouse.Visible = false;
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.btnImportBill);
            this.ribbonPageGroup7.ItemLinks.Add(this.btnExportBill);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Bill";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            // 
            // repositoryItemProgressBar2
            // 
            this.repositoryItemProgressBar2.Name = "repositoryItemProgressBar2";
            // 
            // repositoryItemMarqueeProgressBar1
            // 
            this.repositoryItemMarqueeProgressBar1.Name = "repositoryItemMarqueeProgressBar1";
            // 
            // repositoryItemMarqueeProgressBar2
            // 
            this.repositoryItemMarqueeProgressBar2.MarqueeAnimationSpeed = 500;
            this.repositoryItemMarqueeProgressBar2.Name = "repositoryItemMarqueeProgressBar2";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.btnEdit);
            this.ribbonStatusBar1.ItemLinks.Add(this.btnDeleteRow);
            this.ribbonStatusBar1.ItemLinks.Add(this.btnAddRow);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStatus);
            this.ribbonStatusBar1.ItemLinks.Add(this.barDateTime);
            this.ribbonStatusBar1.ItemLinks.Add(this.btnAddRowSymbol);
            this.ribbonStatusBar1.ItemLinks.Add(this.btnDeleteRowSymbol);
            this.ribbonStatusBar1.ItemLinks.Add(this.btnDone);
            this.ribbonStatusBar1.ItemLinks.Add(this.btnImportItem);
            this.ribbonStatusBar1.ItemLinks.Add(this.btnExportItem);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 458);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(883, 31);
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.btnChangePassword);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "Security";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.dataGridView);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 144);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(883, 314);
            this.panel.TabIndex = 5;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(2, 2);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(879, 310);
            this.dataGridView.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(883, 489);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Ribbon = this.ribbonControl1;
            this.RibbonAlwaysAtBack = false;
            this.RibbonVisibility = DevExpress.XtraBars.Ribbon.RibbonVisibility.Visible;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "Warehouse Management";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barStatusLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel)).EndInit();
            this.panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPage rbpSystemMng;
        private DevExpress.XtraBars.Ribbon.RibbonPage rbpManage;
        private DevExpress.XtraBars.BarButtonItem btnExit;
        private DevExpress.XtraBars.BarButtonItem btnInformation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPage rbpWarehouse;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.BarButtonItem btnChangePassword;
        private DevExpress.XtraBars.BarButtonItem btnBackup;
        private DevExpress.XtraBars.BarButtonItem btnRestore;
        private DevExpress.XtraBars.BarButtonItem btnPartnerEmp;
        private DevExpress.XtraBars.BarButtonItem btnSuppiler;
        private DevExpress.XtraBars.BarButtonItem btnProductGroup;
        private DevExpress.XtraBars.BarButtonItem btnProduct;
        private DevExpress.XtraBars.BarButtonItem btnUniEmp;
        private DevExpress.XtraBars.BarButtonItem btnGrant;
        private DevExpress.XtraBars.BarButtonItem btnImport;
        private DevExpress.XtraBars.BarButtonItem btnExport;
        private DevExpress.XtraBars.BarButtonItem btnStock;
        private DevExpress.XtraBars.BarButtonItem btnImportBill;
        private DevExpress.XtraBars.BarButtonItem btnExportBill;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        public DevExpress.XtraBars.BarButtonItem btnConnect;
        private DevExpress.XtraBars.BarButtonItem btnLogin;
        private DevExpress.XtraBars.BarButtonItem btnAddUser;
        private DevExpress.XtraBars.BarButtonItem btnDeleteUser;
        private DevExpress.XtraBars.BarButtonItem btnEdit;
        private DevExpress.XtraBars.BarButtonItem btnDeleteRow;
        private DevExpress.XtraBars.BarButtonItem btnAddRow;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.PanelControl panel;
        private System.Windows.Forms.DataGridView dataGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraBars.BarStaticItem barDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar2;
        private DevExpress.XtraBars.BarEditItem barStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar barStatusLoading;
        private DevExpress.XtraBars.BarButtonItem btnDone;
        private DevExpress.XtraBars.BarButtonItem btnAddRowSymbol;
        private DevExpress.XtraBars.BarButtonItem btnDeleteRowSymbol;
        private DevExpress.XtraBars.BarButtonItem btnImportItem;
        private DevExpress.XtraBars.BarButtonItem btnExportItem;
    }
}

