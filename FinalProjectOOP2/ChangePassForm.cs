﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BussinessLogic;

namespace FinalProjectOOP2
{
    public partial class ChangePassForm : Form
    {
        public ChangePassForm()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (!TestInput())
            {
                return;
            }

            DTOEmployee nvCu = DatabaseHelper.GetLoggedInUser();
            DTOEmployee nvMoi = DatabaseHelper.GetLoggedInUser();
            nvMoi.Password = textBoxNew.Text;

            BUSEmployee busEmp = new BUSEmployee();
            busEmp.UpdateEmployee(nvCu, nvMoi);
        }

        private bool TestInput()
        {
            if (textBoxOld.Text != DatabaseHelper.GetLoggedInUser().Password)
            {
                MessageBox.Show("Wrong Old Password!!!");
                textBoxOld.Focus();
                return false;
            }

            if (textBoxOld.Text == textBoxNew.Text)
            {
                MessageBox.Show("Old password can not be the same as New password!!!");
                textBoxNew.Focus();
                return false;
            }

            if (textBoxNew.Text != textBoxConfirm.Text)
            {
                MessageBox.Show("2 passwords are not the same!!!");
                textBoxNew.Focus();
                return false;
            }

            return true;
        }
    }
}
