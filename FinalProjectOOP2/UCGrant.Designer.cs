﻿namespace FinalProjectOOP2
{
    partial class UCGrant
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxSID = new System.Windows.Forms.TextBox();
            this.txtBoxEID = new System.Windows.Forms.TextBox();
            this.txtBoxName = new System.Windows.Forms.TextBox();
            this.txtBoxUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxAdmin = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtBoxSID
            // 
            this.txtBoxSID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxSID.Enabled = false;
            this.txtBoxSID.Location = new System.Drawing.Point(0, 0);
            this.txtBoxSID.Name = "txtBoxSID";
            this.txtBoxSID.Size = new System.Drawing.Size(100, 20);
            this.txtBoxSID.TabIndex = 0;
            // 
            // txtBoxEID
            // 
            this.txtBoxEID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxEID.Enabled = false;
            this.txtBoxEID.Location = new System.Drawing.Point(100, 0);
            this.txtBoxEID.Name = "txtBoxEID";
            this.txtBoxEID.Size = new System.Drawing.Size(100, 20);
            this.txtBoxEID.TabIndex = 1;
            // 
            // txtBoxName
            // 
            this.txtBoxName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxName.Enabled = false;
            this.txtBoxName.Location = new System.Drawing.Point(200, 0);
            this.txtBoxName.Name = "txtBoxName";
            this.txtBoxName.Size = new System.Drawing.Size(100, 20);
            this.txtBoxName.TabIndex = 2;
            // 
            // txtBoxUsername
            // 
            this.txtBoxUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxUsername.Enabled = false;
            this.txtBoxUsername.Location = new System.Drawing.Point(300, 0);
            this.txtBoxUsername.Name = "txtBoxUsername";
            this.txtBoxUsername.Size = new System.Drawing.Size(100, 20);
            this.txtBoxUsername.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(400, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.TabIndex = 4;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxAdmin
            // 
            this.checkBoxAdmin.AutoSize = true;
            this.checkBoxAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAdmin.Location = new System.Drawing.Point(419, 2);
            this.checkBoxAdmin.Name = "checkBoxAdmin";
            this.checkBoxAdmin.Size = new System.Drawing.Size(44, 17);
            this.checkBoxAdmin.TabIndex = 5;
            this.checkBoxAdmin.Text = "Yes";
            this.checkBoxAdmin.UseVisualStyleBackColor = true;
            this.checkBoxAdmin.CheckedChanged += new System.EventHandler(this.checkBoxAdmin_CheckedChanged);
            // 
            // UCGrant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkBoxAdmin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBoxUsername);
            this.Controls.Add(this.txtBoxName);
            this.Controls.Add(this.txtBoxEID);
            this.Controls.Add(this.txtBoxSID);
            this.Name = "UCGrant";
            this.Size = new System.Drawing.Size(483, 23);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxSID;
        private System.Windows.Forms.TextBox txtBoxEID;
        private System.Windows.Forms.TextBox txtBoxName;
        private System.Windows.Forms.TextBox txtBoxUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxAdmin;
    }
}
