﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogic;
using DTO;

namespace FinalProjectOOP2
{
    public partial class ExportForm : Form
    {
        List<DTO.DTOProduct> dsHang = DatabaseHelper.GetProductList().ToList();
        
        public ExportForm()
        {
            InitializeComponent();
        }

        private void ExportForm_Load(object sender, EventArgs e)
        {
            DTO.DTOEmployee user = DatabaseHelper.GetLoggedInUser();

            textBoxEID.Text = user.EID;
            textBoxSID.Text = user.Supplier_id;
            textBoxOBID.Text = DatabaseHelper.GetOutBillList().Count.ToString();

            comboBoxName.DataSource = dsHang;
            comboBoxName.DisplayMember = "Name";
            comboBoxName.Text = comboBoxName.Items[0].ToString();

            textBoxDes.Text = dsHang[0].Description;
            textBoxPID.Text = dsHang[0].PID;
            textBoxStorage.Text = dsHang[0].Quantity.ToString();
            textBoxMinStor.Text = dsHang[0].MinQuantity.ToString();
            textBoxMaxLoad.Text = (dsHang[0].Quantity - dsHang[0].MinQuantity).ToString();
            textBoxRemain.Text = (dsHang[0].Quantity - dsHang[0].MinQuantity).ToString();
        }

        private void comboBoxName_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = comboBoxName.SelectedIndex;
            textBoxDes.Text = dsHang[idx].Description;
            textBoxPID.Text = dsHang[idx].PID;
            textBoxStorage.Text = dsHang[idx].Quantity.ToString();
            textBoxMinStor.Text = dsHang[idx].MinQuantity.ToString();
            textBoxMaxLoad.Text = (dsHang[idx].Quantity - dsHang[idx].MinQuantity).ToString();
            int loadout = 0;
            int.TryParse(textBoxNumOut.Text, out loadout);
            textBoxRemain.Text = (dsHang[idx].Quantity - dsHang[idx].MinQuantity).ToString();
        }

        private void textBoxNumOut_TextChanged(object sender, EventArgs e)
        {
            int loadout = 0;
            int idx = comboBoxName.SelectedIndex;
            int.TryParse(textBoxNumOut.Text, out loadout);
            textBoxRemain.Text = (dsHang[idx].Quantity - dsHang[idx].MinQuantity - loadout).ToString();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(textBoxRemain.Text) >= 0)
            {
                int quantity = 0;
                if (int.TryParse(textBoxNumOut.Text, out quantity))
                {
                    DTOProduct oldProduct = dsHang[comboBoxName.SelectedIndex];
                    DTOProduct newProduct = dsHang[comboBoxName.SelectedIndex];
                    newProduct.Quantity -= quantity;
                    dsHang[comboBoxName.SelectedIndex] = newProduct;

                    BUSProduct busProduct = new BUSProduct();
                    busProduct.UpdateProduct(oldProduct, newProduct);

                    BUSOutProduct busOutProduct = new BUSOutProduct();
                    DTOOutProduct outProduct = new DTOOutProduct();
                    outProduct.OBID = textBoxOBID.Text;
                    outProduct.PID = textBoxPID.Text;
                    outProduct.Quantity = quantity;
                    busOutProduct.AddNewOutProduct(outProduct);

                    BUSOutBill busOutBill = new BUSOutBill();
                    DTOOutBill outBill = new DTOOutBill();
                    outBill.Date = DateTime.Now;
                    outBill.EID = textBoxEID.Text;
                    outBill.OBID = textBoxOBID.Text;
                    outBill.Reason = textBoxReason.Text;
                    outBill.Supplier_id = textBoxSID.Text;
                    busOutBill.AddNewOutBill(outBill);
                }
                else
                {
                    MessageBox.Show("Please input correct number!!!");
                    textBoxNumOut.Focus();
                }
            }
            else
            {
                MessageBox.Show("You shouldn't load out more than " + textBoxMaxLoad.Text);
            }
        }
    }
}
