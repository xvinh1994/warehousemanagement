﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using DTO;
using BussinessLogic;
using System.ComponentModel;

namespace FinalProjectOOP2
{
    class DatabaseHelper
    {
        public static bool isUser = false;
        public static bool isAdmin = false;
        public static SqlConnection Connection = new SqlConnection();

        private static DTOEmployee user = null;

        public static bool hasConnection()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=""C:\Users\Vinh\Documents\Visual Studio 2013\Projects\FinalProjectOOP2\FinalProjectOOP2\QLKho.mdf"";Integrated Security=True";
            
            try
            {
                conn.Open();
                Connection = conn;

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return false;
        }

        public static void SetLoggedInUser(DTOEmployee inUser)
        {
            user = inUser;
        }

        public static DTOEmployee GetLoggedInUser()
        {
            return user;
        }

        public static void SetUserList(BindingList<DTOEmployee> danhsach)
        {
            DatabaseTemporaryStorage.dsNVien = danhsach;
        }

        public static bool AcceptedEmployeeRow(DataGridViewRow row)
        {
            if (row.Cells["EID"].FormattedValue.ToString() == "") return false;
            if (DatabaseHelper.GetLoggedInUser().Supplier_id != row.Cells["Supplier_id"].FormattedValue.ToString())
                return false;
            if (row.Cells["Name"].FormattedValue.ToString() == "") return false;
            if (row.Cells["Username"].FormattedValue.ToString() == "") return false;
            if (row.Cells["Password"].FormattedValue.ToString() == "") return false;
            return true;
        }

        public static bool AcceptedSupplierRow(DataGridViewRow row)
        {
            if (row.Cells["Supplier_id"].FormattedValue.ToString() == "") return false;
            if (row.Cells["Name"].FormattedValue.ToString() == "") return false;
            if (row.Cells["Address"].FormattedValue.ToString() == "") return false;
            if (row.Cells["PhoneNumber"].FormattedValue.ToString() == "") return false;
            return true;
        }

        public static DTOEmployee ConvertRowToEmployee(DataGridViewRow row)
        {
            DTOEmployee emp = new DTOEmployee();
            emp.EID = row.Cells["EID"].FormattedValue.ToString();
            emp.Supplier_id = DatabaseHelper.GetLoggedInUser().Supplier_id;
            emp.Name = row.Cells["Name"].FormattedValue.ToString();
            emp.Username = row.Cells["Username"].FormattedValue.ToString();
            emp.Password = row.Cells["Password"].FormattedValue.ToString();
            emp.IsAdmin = false;

            return emp;
        }

        public static DTOSupplier ConvertRowToSupplier(DataGridViewRow row)
        {
            DTOSupplier sup = new DTOSupplier();

            sup.Supplier_id = row.Cells["Supplier_id"].FormattedValue.ToString();
            sup.Name = row.Cells["Name"].FormattedValue.ToString();
            sup.Address = row.Cells["Address"].FormattedValue.ToString();
            sup.PhoneNumber = row.Cells["PhoneNumber"].FormattedValue.ToString();

            return sup;
        }

        #region Load Database
        public static void LoadAllData()
        {
            LoadInProduct();
            LoadOutProduct();
            LoadInBill();
            LoadOutBill();
            LoadSupplier();
            LoadProduct();
        }

        private static void LoadInProduct()
        {
            BUSInProduct busInProduct = new BUSInProduct();
            DatabaseTemporaryStorage.dsHangNhap = busInProduct.GetAll();
        }

        public static BindingList<DTOInProduct> LoadInProductFromSupplier(DTOEmployee nvien)
        {
            return new BussinessLogic.BUSInProduct().GetAllFromSupplier(nvien);
        }

        private static void LoadOutProduct()
        {
            BUSOutProduct busOutProduct = new BUSOutProduct();
            DatabaseTemporaryStorage.dsHangXuat = busOutProduct.GetAll();
        }

        private static void LoadInBill()
        {
            BUSInBill busInBill = new BUSInBill();
            DatabaseTemporaryStorage.dsHoaDonNhap = busInBill.GetAll();
        }

        private static void LoadOutBill()
        {
            BUSOutBill busOutBill = new BUSOutBill();
            DatabaseTemporaryStorage.dsHoaDonXuat = busOutBill.GetAll();
        }

        private static void LoadSupplier()
        {
            BUSSupplier busSupplier = new BUSSupplier();
            DatabaseTemporaryStorage.dsNhaCungCap = busSupplier.GetAll();
        }

        private static void LoadProduct()
        {
            BUSProduct busProduct = new BUSProduct();
            DatabaseTemporaryStorage.dsSanPham = busProduct.GetAll();
        }
        #endregion

        #region Get Data
        public static BindingList<DTOInBill> GetInBillList()
        {
            return DatabaseTemporaryStorage.dsHoaDonNhap;
        }

        public static BindingList<DTOEmployee> GetUserList()
        {
            return DatabaseTemporaryStorage.dsNVien;
        }

        public static BindingList<DTOInProduct> GetInProductList()
        {
            return DatabaseTemporaryStorage.dsHangNhap;
        }

        public static BindingList<DTOOutBill> GetOutBillList()
        {
            return DatabaseTemporaryStorage.dsHoaDonXuat;
        }

        public static BindingList<DTOOutProduct> GetOutProductList()
        {
            return DatabaseTemporaryStorage.dsHangXuat;
        }

        public static BindingList<DTOProduct> GetProductList()
        {
            return DatabaseTemporaryStorage.dsSanPham;
        }

        public static BindingList<DTOSupplier> GetSuppliers()
        {
            return DatabaseTemporaryStorage.dsNhaCungCap;
        }
        #endregion
    }
}
