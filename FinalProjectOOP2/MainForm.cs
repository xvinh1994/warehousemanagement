﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogic;
using DTO;
using System.Threading;
using System.Collections.ObjectModel;

namespace FinalProjectOOP2
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        Thread t_timer = null;
        Thread t_load = null;
        Thread t_save = null;
        bool EmployeeClicked = false, SupplierClicked = false;
        bool EditClicked = false, DelClicked = false, AddClicked = false;
        int StartAddRowIndexGridView = 0, StartAddIndexInList = 0;
        List<int> DeleteRowIndexList = new List<int>();
        DataGridView prevTable = new DataGridView();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += timer_Elapsed;
            t_timer = new Thread(new ThreadStart(timer.Start));
            t_timer.Start();

            if (DatabaseHelper.isAdmin) btnExport.Enabled = true;
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            barDateTime.Caption = DateTime.Now.ToString();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (t_timer != null) t_timer.Abort();
            if (t_save != null) t_save.Abort();
            if (DatabaseHelper.Connection.State == ConnectionState.Open) DatabaseHelper.Connection.Close();
        }

        #region System Manager Buttons
        private void btnConnect_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            barStatus.Caption = "Connecting";
            barStatusLoading.Stopped = false;

            if (DatabaseHelper.hasConnection())
            {
                btnLogin.Enabled = true;
                btnConnect.Enabled = false;
                btnConnect.Caption = "Connected";

                t_load = new Thread(new ThreadStart(DatabaseHelper.LoadAllData));
                t_load.Start();
                MessageBox.Show("Connection Succeed. Please Login!!!");

                barStatus.Caption = "Waiting mode";
                barStatusLoading.Stopped = true;
            }
            else
            {
                MessageBox.Show("No connection!!!");
            }
        }

        private void btnLogin_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            barStatus.Caption = "Signing In";
            barStatusLoading.Stopped = false;

            Login loginFrm = new Login();
            loginFrm.ShowDialog();

            if (DatabaseHelper.isUser)
            {
                EnableFeaturesUser();
                btnLogin.Enabled = false;
            }

            if (DatabaseHelper.isAdmin)
            {
                EnableFeaturesAdmin();
            }

            barStatus.Caption = "Waiting mode";
            barStatusLoading.Stopped = true;
        }

        private void btnInformation_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DatabaseHelper.GetLoggedInUser() != null)
            {
                var user = DatabaseHelper.GetLoggedInUser();
                MessageBox.Show("- EID: " + user.EID + "\r\n- SID: " + user.Supplier_id + "\r\n- Name: " + user.Name + "\r\n- Username: " + user.Username + "\r\n");
            }

            if (DatabaseHelper.Connection != null)
            {
                if (DatabaseHelper.Connection.State == ConnectionState.Closed) DatabaseHelper.Connection.Open();
                MessageBox.Show("Server Information: " + DatabaseHelper.Connection.ServerVersion);
                DatabaseHelper.Connection.Close();
            }
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnGrant_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GrantForm grantFrm = new GrantForm();
            grantFrm.ShowDialog();
        }

        private void btnChangePassword_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ChangePassForm changeFrm = new ChangePassForm();
            changeFrm.ShowDialog();
        }

        private void btnDeleteUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DelUserForm delFrm = new DelUserForm();
            delFrm.ShowDialog();
        }

        private void btnAddUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddUserForm addFrm = new AddUserForm();
            addFrm.ShowDialog();
        }
        #endregion

        #region Manage Buttons
        private void btnPartnerEmp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue"))
                    return;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            EmployeeClicked = true;
            SupplierClicked = false;

            if (DatabaseHelper.isAdmin)
            {
                dataGridView.DataSource = DatabaseHelper.GetUserList();
                btnEdit.Enabled = true;
            }
            else
            {
                DTOEmployee user = DatabaseHelper.GetLoggedInUser();
                dataGridView.DataSource = DatabaseHelper.GetUserList().ToList().FindAll(x => x.Supplier_id == user.Supplier_id);
            }

            dataGridView.Columns[4].Visible = false;

            btnDeleteRow.Enabled = true;
            btnAddRow.Enabled = true;
        }

        private void btnSuppiler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue?"))
                    return;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            SupplierClicked = true;
            EmployeeClicked = false;

            dataGridView.DataSource = DatabaseHelper.GetSuppliers();
            btnEdit.Enabled = true;
        }

        private void btnUniEmp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue?"))
                    return;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            dataGridView.DataSource = DatabaseHelper.GetUserList().ToList().FindAll(x => x.IsAdmin == true && x!= DatabaseHelper.GetLoggedInUser());

            btnEdit.Enabled = true;
            btnAddRow.Enabled = true;
            btnDeleteRow.Enabled = true;
        }

        private void btnStock_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue?"))
                    return;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            if (DatabaseHelper.isAdmin)
                dataGridView.DataSource = DatabaseHelper.GetProductList();
            else
            {
                DTOEmployee user = DatabaseHelper.GetLoggedInUser();
                dataGridView.DataSource = DatabaseHelper.GetProductList().ToList().FindAll(x => x.Supplier_id == user.Supplier_id);
            }
        }

        private void btnImport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue?"))
                    return;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnDone.Enabled = false;
            btnImportItem.Enabled = true;
            btnExportItem.Enabled = false;

            if (DatabaseHelper.isAdmin) dataGridView.DataSource = DatabaseHelper.GetInProductList();
            else dataGridView.DataSource = DatabaseHelper.LoadInProductFromSupplier(DatabaseHelper.GetLoggedInUser());
        }

        private void btnExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue?"))
                    return;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnDone.Enabled = false;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = true;

            dataGridView.DataSource = DatabaseHelper.GetOutProductList();
        }
        #endregion

        #region Statusbar Buttons
        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            prevTable.DataSource = dataGridView.DataSource;

            EditClicked = true;
            AddClicked = false;
            DelClicked = false;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnDone.Enabled = true;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            barStatus.Caption = "Editing Mode";
            barStatusLoading.Stopped = false;

            dataGridView.ReadOnly = false;
            if(EmployeeClicked) dataGridView.Columns[5].ReadOnly = true;
        }

        private void btnDeleteRow_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteRowIndexList.Clear();
            prevTable.DataSource = dataGridView.DataSource;

            DelClicked = true;
            AddClicked = false;
            EditClicked = false;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnAddRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btnDeleteRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            btnDeleteRow.Enabled = false;
            btnDone.Enabled = true;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            barStatus.Caption = "Deleting Mode";
            barStatusLoading.Stopped = false;

            dataGridView.ReadOnly = false;
            dataGridView.AllowUserToDeleteRows = true;
        }

        private void btnAddRow_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StartAddRowIndexGridView = dataGridView.RowCount;
            prevTable.DataSource = dataGridView.DataSource;

            AddClicked = true;
            EditClicked = false;
            DelClicked = false;

            btnEdit.Enabled = false;
            btnAddRow.Enabled = false;
            btnAddRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            btnDeleteRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btnDeleteRow.Enabled = false;
            btnDone.Enabled = true;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            barStatus.Caption = "Adding Mode";
            barStatusLoading.Stopped = false;

            dataGridView.ReadOnly = false;
            dataGridView.AllowUserToAddRows = true;

            if (EmployeeClicked)
            {
                dataGridView.Columns[4].Visible = true;
                dataGridView.Columns[5].ReadOnly = true;
                StartAddIndexInList = DatabaseHelper.GetUserList().Count;
            }
        }

        private void btnDone_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DatabaseHelper.GetLoggedInUser().IsAdmin)
            {
                btnEdit.Enabled = true;
                btnAddRow.Enabled = true;
                btnDeleteRow.Enabled = true;
            }
            else
            {
                if (EmployeeClicked)
                {
                    btnAddRow.Enabled = true;
                    btnDeleteRow.Enabled = true;
                }

                if (SupplierClicked) btnEdit.Enabled = true;
            }

            if (EmployeeClicked) dataGridView.Columns[4].Visible = false;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.ReadOnly = true;

            btnDone.Enabled = false;
            btnAddRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btnDeleteRowSymbol.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;

            barStatus.Caption = "Waiting Mode";
            barStatusLoading.Stopped = true;

            t_save = new Thread(new ThreadStart(SaveToDatabase));
            t_save.Start();

            EditClicked = false;
            DelClicked = false;
            AddClicked = false;
        }

        private void btnAddRowSymbol_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EmployeeClicked)
            {
                DatabaseHelper.GetUserList().Add(new DTOEmployee());
                dataGridView.DataSource = DatabaseHelper.GetUserList();
            }

            if (SupplierClicked)
            {
                DatabaseHelper.GetSuppliers().Add(new DTOSupplier());
                dataGridView.DataSource = DatabaseHelper.GetSuppliers();
            }
        }

        private void btnDeleteRowSymbol_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (dataGridView.RowCount > 0)
            {
                int idx = dataGridView.SelectedRows[0].Index;
                IBindingList ibindinglist = null;

                if (EmployeeClicked)
                {
                    DTOEmployee empRemoved = DatabaseHelper.ConvertRowToEmployee(dataGridView.SelectedRows[0]);
                    //DatabaseHelper.GetUserList().Remove(empRemoved);
                    DeleteRowIndexList.Add(DatabaseHelper.GetUserList().ToList().FindIndex(x => x == empRemoved));
                    ibindinglist = DatabaseHelper.GetUserList();
                    ibindinglist.RemoveAt(idx);
                }

                if (SupplierClicked)
                {
                    DTOSupplier suppRemoved = DatabaseHelper.ConvertRowToSupplier(dataGridView.SelectedRows[0]);
                    //DatabaseHelper.GetSuppliers().Remove(suppRemoved);
                    DeleteRowIndexList.Add(DatabaseHelper.GetSuppliers().ToList().FindIndex(x => x == suppRemoved));
                    ibindinglist = DatabaseHelper.GetSuppliers();
                    ibindinglist.RemoveAt(idx);
                }

                dataGridView.DataSource = ibindinglist;
            }
        }

        private void btnImportItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            barStatus.Caption = "Importing Product mode";
            barStatusLoading.Stopped = false;

            ImportForm frmImport = new ImportForm();
            frmImport.ShowDialog();

            barStatusLoading.Stopped = true;

            if (DatabaseHelper.isAdmin) dataGridView.DataSource = DatabaseHelper.GetInProductList();
            else dataGridView.DataSource = DatabaseHelper.LoadInProductFromSupplier(DatabaseHelper.GetLoggedInUser());
        }

        private void btnExportItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            barStatus.Caption = "Exporting Product mode";
            barStatusLoading.Stopped = false;

            ExportForm frmExport = new ExportForm();
            frmExport.ShowDialog();

            barStatusLoading.Stopped = true;

            dataGridView.DataSource = DatabaseHelper.GetOutProductList();
        }
        #endregion

        #region Warehouse Buttons
        private void btnImportBill_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue?"))
                    return;

            btnEdit.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnAddRow.Enabled = false;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            if (DatabaseHelper.isAdmin)
                dataGridView.DataSource = DatabaseHelper.GetInBillList();
            else
            {
                DTOEmployee user = DatabaseHelper.GetLoggedInUser();
                dataGridView.DataSource = DatabaseHelper.GetInBillList().ToList().FindAll(x => x.Supplier_id == user.Supplier_id);
            }
        }

        private void btnExportBill_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (EditClicked || AddClicked || DelClicked)
                if (!ConfirmToDo("There is table in Editing/Adding/Deleting Mode. Do you want to continue?"))
                    return;

            btnEdit.Enabled = false;
            btnDeleteRow.Enabled = false;
            btnAddRow.Enabled = false;
            btnImportItem.Enabled = false;
            btnExportItem.Enabled = false;

            if (DatabaseHelper.isAdmin)
                dataGridView.DataSource = DatabaseHelper.GetOutBillList();
            else
            {
                DTOEmployee user = DatabaseHelper.GetLoggedInUser();
                dataGridView.DataSource = DatabaseHelper.GetOutBillList().ToList().FindAll(x => x.Supplier_id == user.Supplier_id);
            }
        }
        #endregion

        #region Functions in Form
        private void EnableFeaturesAdmin()
        {
            btnGrant.Enabled = true;
            btnDeleteUser.Enabled = true;
            btnAddUser.Enabled = true;
            ribbonPageGroup10.Visible = true;
        }

        private void EnableFeaturesUser()
        {
            ribbonPageGroup3.Visible = true;
            ribbonPageGroup4.Visible = true;
            rbpManage.Visible = true;
            rbpWarehouse.Visible = true;
        }

        private bool ConfirmToDo(string str)
        {
            if (MessageBox.Show(str, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                return true;
            return false;
        }

        private void SaveToDatabase()
        {
            if (EmployeeClicked) SaveEmployeeToDatabase();
            else if (SupplierClicked) SaveSupplierToDatabase();
        }

        private void SaveSupplierToDatabase()
        {
            BUSSupplier busSupplier = new BUSSupplier();

            if (EditClicked == true)
            {
                for (int i = 0; i < dataGridView.RowCount; i++)
                {
                    if (DatabaseHelper.AcceptedSupplierRow(dataGridView.Rows[i]))
                    {
                        DTOSupplier newSupp = DatabaseHelper.ConvertRowToSupplier(dataGridView.Rows[i]);
                        DTOSupplier oldSupp = DatabaseHelper.ConvertRowToSupplier(prevTable.Rows[i]);

                        if (newSupp != oldSupp)
                        {
                            busSupplier.DeleteSelectedSupplier(oldSupp);
                            busSupplier.AddNewSupplier(newSupp);

                            int idx = DatabaseHelper.GetSuppliers().ToList().FindIndex(x => x == oldSupp);
                            DatabaseHelper.GetSuppliers()[idx] = newSupp;
                        }
                    }
                    else
                    {
                        dataGridView.Rows[i].SetValues(prevTable.Rows[i].Cells);
                    }
                }
            }
            else if (DelClicked == true)
            {
                List<DTOSupplier> removeList = new List<DTOSupplier>();
                foreach (int idx in DeleteRowIndexList)
                {
                    DTOSupplier suppRemoved = DatabaseHelper.GetSuppliers()[idx];
                    removeList.Add(suppRemoved);
                }
                foreach (DTOSupplier suppRemoved in removeList)
                {
                    busSupplier.DeleteSelectedSupplier(suppRemoved);
                    DatabaseHelper.GetSuppliers().Remove(suppRemoved);
                }
            }
            else if (AddClicked == true)
            {
                for (int i = StartAddRowIndexGridView; i < dataGridView.RowCount; i++)
                {
                    if (DatabaseHelper.AcceptedSupplierRow(dataGridView.Rows[i]))
                    {
                        DTOSupplier newSupp = DatabaseHelper.ConvertRowToSupplier(dataGridView.Rows[i]);
                        DatabaseHelper.GetSuppliers()[StartAddIndexInList++] = newSupp;
                        busSupplier.AddNewSupplier(newSupp);
                    }
                    else
                    {
                        dataGridView.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        private void SaveEmployeeToDatabase()
        {
            BUSEmployee busEmployee = new BUSEmployee();

            if (EditClicked == true)
            {
                for (int i = 0; i < dataGridView.RowCount; i++)
                {
                    if (DatabaseHelper.AcceptedEmployeeRow(dataGridView.Rows[i]))
                    {
                        DTOEmployee newEmp = DatabaseHelper.ConvertRowToEmployee(dataGridView.Rows[i]);
                        DTOEmployee oldEmp = DatabaseHelper.ConvertRowToEmployee(prevTable.Rows[i]);

                        if (newEmp != oldEmp)
                        {
                            busEmployee.DeleteUser(oldEmp);
                            busEmployee.AddNewUser(newEmp);

                            int idx = DatabaseHelper.GetUserList().ToList().FindIndex(x => x == oldEmp);
                            DatabaseHelper.GetUserList()[idx] = newEmp;
                        }
                    }
                    else
                    {
                        dataGridView.Rows[i].SetValues(prevTable.Rows[i].Cells);
                    }
                }
            }
            else if (DelClicked == true)
            {
                List<DTOEmployee> removeList = new List<DTOEmployee>();
                foreach (int idx in DeleteRowIndexList)
                {
                    DTOEmployee empRemoved = DatabaseHelper.GetUserList()[idx];
                    removeList.Add(empRemoved);
                }
                foreach (DTOEmployee empRemoved in removeList)
                {
                    busEmployee.DeleteUser(empRemoved);
                    DatabaseHelper.GetUserList().Remove(empRemoved);
                }
            }
            else if (AddClicked == true)
            {
                for (int i = StartAddRowIndexGridView; i < dataGridView.RowCount; i++)
                {
                    if (DatabaseHelper.AcceptedEmployeeRow(dataGridView.Rows[i]))
                    {
                        DTOEmployee newEmp = DatabaseHelper.ConvertRowToEmployee(dataGridView.Rows[i]);
                        DatabaseHelper.GetUserList()[StartAddIndexInList++] = newEmp;
                        busEmployee.AddNewUser(newEmp);
                    }
                    else
                    {
                        dataGridView.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
        }
        #endregion
    }
}
