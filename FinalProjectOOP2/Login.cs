﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogic;
using DTO;

namespace FinalProjectOOP2
{
    public partial class Login : Form
    {
        BindingList<DTOEmployee> dsNVien = new BindingList<DTOEmployee>();

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            BUSEmployee getEmployee = new BUSEmployee();
            dsNVien = getEmployee.GetAll();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (textBoxUser.Text == "")
            {
                MessageBox.Show("Please input username!!!");
                textBoxUser.Focus();
                return;
            }

            if (isUser())
            {
                DatabaseHelper.isUser = true;
                this.Close();
            }
            else
            {
                textBoxPass.Text = "";
                textBoxUser.Text = "";
                MessageBox.Show("Please check your information!!!");
            }
        }

        private bool isUser()
        {
            int idx = -1;
            if ((idx = dsNVien.ToList().FindIndex(x => x.Username == textBoxUser.Text && x.Password == textBoxPass.Text)) != -1)
            {
                DatabaseHelper.SetLoggedInUser(dsNVien[idx]);

                DatabaseHelper.SetUserList(dsNVien);
                if (dsNVien[idx].IsAdmin) DatabaseHelper.isAdmin = true;
                return true;
            }

            return false;
        }
    }
}
