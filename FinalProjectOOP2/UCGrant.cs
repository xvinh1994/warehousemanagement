﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;

namespace FinalProjectOOP2
{
    public partial class UCGrant : UserControl
    {
        DTOEmployee emp = null;
        public bool isChanged = false;

        public UCGrant()
        {
            InitializeComponent();
        }

        public UCGrant(DTOEmployee nvien)
        {
            InitializeComponent();

            txtBoxEID.Text = nvien.EID;
            txtBoxName.Text = nvien.Name;
            txtBoxSID.Text = nvien.Supplier_id;
            txtBoxUsername.Text = nvien.Username;
            checkBoxAdmin.Checked = nvien.IsAdmin;

            emp = nvien;
        }

        public DTOEmployee GetEmployee()
        {
            return emp;
        }

        private void checkBoxAdmin_CheckedChanged(object sender, EventArgs e)
        {
            isChanged = !isChanged;
        }
    }
}
