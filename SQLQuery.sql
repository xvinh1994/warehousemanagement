﻿create table SUPPLIER
(
	SID nvarchar(16) NOT NULL,
	NAME nvarchar(50),
	ADDRESS nvarchar(100),
	PHONE_NUMBER nvarchar(13),
	PRIMARY KEY(SID)
);

create table EMPLOYEE
(
	EID nvarchar(16) NOT NULL,
	SUPPLIER_ID nvarchar(16),
	NAME nvarchar(50),
	USERNAME nvarchar(16),
	PASSWORD nvarchar(16),
	ISADMIN varchar(5),
	PRIMARY KEY(EID, SUPPLIER_ID)
);

create table PRODUCT
(
	PID nvarchar(16) NOT NULL,
	SID nvarchar(16) NOT NULL,
	NAME nvarchar(50),
	DESCRIPTION nvarchar(20),
	IN_PRICE float,
	OUT_PRICE float,
	QUANTITY int,
	MIN_QUANTITY int,
	NOTE nvarchar(20),
	PRIMARY KEY(PID)
);

create table IN_PRODUCT
(
	PID nvarchar(16) NOT NULL,
	IBID int NOT NULL,
	QUANTITY int,
	PRIMARY KEY(IBID, PID)
);

create table OUT_PRODUCT
(
	PID nvarchar(16) NOT NULL,
	OBID int NOT NULL,
	QUANTITY int,
	PRIMARY KEY(OBID, PID)
);


create table IN_BILL
(
	IBID int NOT NULL,
	DATE date,
	REASON nvarchar(50),
	EID nvarchar(16),
	SID nvarchar(16),
	PRIMARY KEY(IBID)
);

create table OUT_BILL
(
	OBID int NOT NULL,
	DATE date,
	REASON nvarchar(50),
	EID nvarchar(16),
	SID nvarchar(16),
	PRIMARY KEY(OBID)
);

alter table EMPLOYEE
add constraint FK_EMPLOYEE_SUPPLIER_ID foreign key (SUPPLIER_ID) references SUPPLIER(SID);

alter table PRODUCT
add constraint FK_PRODUCT_SID foreign key (SID) references SUPPLIER(SID);

alter table IN_PRODUCT
add constraint FK_IN_PRODUCT_PID foreign key (PID) references PRODUCT(PID);
alter table IN_PRODUCT
add constraint FK_IN_PRODUCT_IBID foreign key (IBID) references IN_BILL(IBID);

alter table OUT_PRODUCT
add constraint FK_OUT_PRODUCT_PID foreign key (PID) references PRODUCT(PID);
alter table OUT_PRODUCT
add constraint FK_OUT_PRODUCT_OBID foreign key (OBID) references OUT_BILL(OBID);

alter table IN_BILL
add constraint FK_IN_BILL_EID_SID foreign key (EID,SID) references EMPLOYEE(EID,SUPPLIER_ID);

alter table OUT_BILL
add constraint FK_OUT_BILL_EID_SID foreign key (EID, SID) references EMPLOYEE(EID, SUPPLIER_ID);

insert into SUPPLIER values ('UP_SAIGON', N'Sài Gòn CO.OP', N'123 Điện Biên Phủ, District 3, Hồ Chí Minh City', '01654 000 111');
insert into SUPPLIER values ('UP_THAILAND', N'Metro Thailand (Hồ Chí Minh)', N'Floor 25, Bitexco Tower, District 1, Hồ Chí Minh City', '08367 213 872');
insert into SUPPLIER values ('UH_DONGNAI', N'BigC Đồng Nai', N'243 Nguyễn Trãi, Biên Hòa City, Đồng Nai Province', '01235 826 726');
insert into SUPPLIER values ('UH_DANANG', N'Shop and Go Đà Nẵng', N'113 Lãnh Bình Thăng, Liên Chiểu District, Da Nang City', '04324 336 872');
insert into SUPPLIER values ('UF_HANOI', N'Citimart Hà Nội', N'889 Hai Bà Trưng, Hà Đông District, Ha Noi City ', '02364 283 764');
insert into SUPPLIER values ('UF_BINHDUONG', N'Lottemart Bình Dương', N'141 Trần Hưng Đạo, Dĩ An District, Binh Duong Province', '02348 531 871');
insert into SUPPLIER values ('UU_SAIGON', N'Unilever Sài Gòn', N'Floor 1, Bitexco Tower, District 1, Hồ Chí Minh City', '02653 637 162');

insert into EMPLOYEE values ('UU_SA001', 'UU_SAIGON', N'Vương Trùng Dương', 'admin', 'unilever', 'true');
insert into EMPLOYEE values ('UP_SA001', 'UP_SAIGON', N'Trần Văn Tèo', 'saigon1', 'saigon', 'false');
insert into EMPLOYEE values ('UP_TH001', 'UP_THAILAND', N'Nguyễn Văn Tí', 'thailand1', 'thailand', 'false');
insert into EMPLOYEE values ('UP_TH002', 'UP_THAILAND', N'Đào Thị Thanh Tú', 'thailand2', 'thailand', 'false');
insert into EMPLOYEE values ('UH_DA001', 'UH_DANANG', N'Cao Cao Thanh', 'danang', 'danang', 'false');
insert into EMPLOYEE values ('UH_DO001', 'UH_DONGNAI', N'Đỗ Nhật Nam', 'dongnai1', 'dongnai', 'false');
insert into EMPLOYEE values ('UH_DO002', 'UH_DONGNAI', N'Trần Thiên Long', 'dongnai2', 'dongnai', 'false');
insert into EMPLOYEE values ('UF_HA001', 'UF_HANOI', N'Tô Nữ Thanh Tú', 'hanoi1', 'hanoi', 'false');
insert into EMPLOYEE values ('UF_HA002', 'UF_HANOI', N'Đoàn Dự', 'hanoi2', 'hanoi', 'false');
insert into EMPLOYEE values ('UF_BI001', 'UF_BINHDUONG', N'Tiêu Phong', 'binhduong', 'binhduong', 'false');

insert into PRODUCT values('PSU', 'UP_THAILAND', 'Sunsilk', 'Personal care', '30000', '35000', '600', '300', 'None');
insert into PRODUCT values('PLI', 'UP_THAILAND', 'Lifebouy', 'Personal care', '5000', '6000', '450', '300', 'None');
insert into PRODUCT values('PPS', 'UP_SAIGON', 'P/S', 'Personal care', '17000', '19000', '300', '150', 'None');
insert into PRODUCT values('PCE', 'UP_THAILAND', 'Clear', 'Personal care', '34000', '34500', '248', '300', 'None');
insert into PRODUCT values('PPO', 'UP_SAIGON', 'Ponds', 'Personal care', '100000', '115000', '100', '300', 'None');
insert into PRODUCT values('HOM', 'UH_DANANG', 'Omo', 'Home care', '20000', '22000', '250', '200', 'None');
insert into PRODUCT values('HCO', 'UH_DANANG', 'Comfort', 'Home care', '98000', '100000', '222', '200', 'None');
insert into PRODUCT values('HCI', 'UH_DANANG', 'Cif', 'Home care', '31300', '32000', '200', '150', 'None');
insert into PRODUCT values('HSU', 'UH_DONGNAI', 'Sunlight', 'Home care', '15000', '17000', '109', '200', 'None');
insert into PRODUCT values('FWA', 'UF_HANOI', 'Wall', 'Food and drink', '119000', '122000', '150', '150', 'None');
insert into PRODUCT values('KWN', 'UF_HANOI', 'Knor', 'Food and drink', '6000', '6500', '200', '150', 'None');
insert into PRODUCT values('FLI', 'UF_BINHDUONG', 'Lipton', 'Food and drink', '35000', '40000', '323', '150', 'None');
insert into PRODUCT values('FUF', 'UF_BINHDUONG', 'Unilever Food Solutions', 'Food and drink', '10000', '11000', '408', '150', 'None');
insert into PRODUCT values('FGO', 'UF_HANOI', 'Goute', 'Food and drink', '80000', '81500', '494', '200', 'None');
insert into PRODUCT values('FHD', 'UF_HANOI', 'Hura Deli', 'Food and drink', '49500', '50000', '281', '170', 'None');


