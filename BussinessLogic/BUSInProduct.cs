﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;
using System.ComponentModel;

namespace BussinessLogic
{
    public class BUSInProduct
    {
        public BindingList<DTOInProduct> GetAll()
        {
            return new DAOInProduct().GetAll();
        }

        public BindingList<DTOInProduct> GetAllFromSupplier(DTOEmployee nvien)
        {
            return new DAOInProduct().GetAllFromSupplier(nvien);
        }

        public void AddNewInBill(DTOInProduct hangnhap)
        {
            DAOInProduct user = new DAOInProduct();
            user.AddNewInProduct(hangnhap);
        }

        public void DeleteSelectedInBill(DTOInProduct hangnhap)
        {
            DAOInProduct user = new DAOInProduct();
            user.DeleteSelectedInProduct(hangnhap);
        }

        public void DeleteAllInBill()
        {
            DAOInProduct user = new DAOInProduct();
            user.DeleteAllInProduct();
        }

        public void UpdateInBill(DTOInProduct hangnhapCu, DTOInProduct hangnhapMoi)
        {
            DAOInProduct user = new DAOInProduct();
            user.UpdateInProduct(hangnhapCu, hangnhapMoi);
        }
    }
}
