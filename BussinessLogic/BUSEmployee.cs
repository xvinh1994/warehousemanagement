﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;
using System.ComponentModel;

namespace BussinessLogic
{
    public class BUSEmployee
    {
        public BindingList<DTOEmployee> GetAll()
        {
            return new DAOEmployee().GetAll();
        }

        public void AddNewUser(DTOEmployee nvien)
        {
            DAOEmployee admin = new DAOEmployee();
            admin.AddNewUser(nvien);
        }

        public void DeleteUser(DTOEmployee nvien)
        {
            DAOEmployee admin = new DAOEmployee();
            admin.DeleteUser(nvien);
        }

        public void DeleteAllUser()
        {
            DAOEmployee admin = new DAOEmployee();
            admin.DeleteAllUser();
        }

        public void UpdateEmployee(DTOEmployee nvCu, DTOEmployee nvMoi)
        {
            DAOEmployee admin = new DAOEmployee();
            admin.UpdateUser(nvCu, nvMoi);
        }
    }
}
