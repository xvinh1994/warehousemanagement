﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;
using System.ComponentModel;

namespace BussinessLogic
{
    public class BUSProduct
    {
        public BindingList<DTOProduct> GetAll()
        {
            return new DAOProduct().GetAll();
        }

        public void AddNewProduct(DTOProduct hang)
        {
            DAOProduct user = new DAOProduct();
            user.AddNewProduct(hang);
        }

        public void DeleteSelectedInBill(DTOProduct hang)
        {
            DAOProduct user = new DAOProduct();
            user.DeleteProduct(hang);
        }

        public void DeleteAllProduct()
        {
            DAOProduct user = new DAOProduct();
            user.DeleteAllProduct();
        }

        public void UpdateProduct(DTOProduct hangCu, DTOProduct hangMoi)
        {
            DAOProduct user = new DAOProduct();
            user.UpdateProduct(hangCu, hangMoi);
        }
    }
}
