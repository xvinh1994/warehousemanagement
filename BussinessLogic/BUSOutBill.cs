using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;
using System.ComponentModel;

namespace BussinessLogic
{
    public class BUSOutBill
    {
        public BindingList<DTOOutBill> GetAll()
        {
            return new DAOOutBill().GetAll();
        }

        public void AddNewOutBill(DTOOutBill hoadon)
        {
            DAOOutBill user = new DAOOutBill();
            user.AddNewOutBill(hoadon);
        }

        public void DeleteSelectedOutBill(DTOOutBill hoadon)
        {
            DAOOutBill user = new DAOOutBill();
            user.DeleteSelectedOutBill(hoadon);
        }

        public void DeleteAllOutBill()
        {
            DAOOutBill user = new DAOOutBill();
            user.DeleteAllOutBill();
        }

        public void UpdateOutBill(DTOOutBill hoadonCu, DTOOutBill hoadonMoi)
        {
            DAOOutBill user = new DAOOutBill();
            user.UpdateOutBill(hoadonCu, hoadonMoi);
        }
    }
}

