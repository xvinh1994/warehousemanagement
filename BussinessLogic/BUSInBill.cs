﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;
using System.ComponentModel;

namespace BussinessLogic
{
    public class BUSInBill
    {
        public BindingList<DTOInBill> GetAll()
        {
            return new DAOInBill().GetAll();
        }

        public void AddNewInBill(DTOInBill hoadonnhap)
        {
            DAOInBill admin = new DAOInBill();
            admin.AddNewInBill(hoadonnhap);
        }

        public void DeleteSelectedInBill(DTOInBill hoadonnhap)
        {
            DAOInBill admin = new DAOInBill();
            admin.DeleteSelectedInBill(hoadonnhap);
        }

        public void DeleteAllInBill()
        {
            DAOInBill admin = new DAOInBill();
            admin.DeleteAllInBill();
        }

        public void UpdateInBill(DTOInBill hoadonCu, DTOInBill hoadonMoi)
        {
            DAOInBill user = new DAOInBill();
            user.UpdateInBill(hoadonCu, hoadonMoi);
        }
    }
}
