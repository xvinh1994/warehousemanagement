﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAO;
using System.ComponentModel;

namespace BussinessLogic
{
    public class BUSSupplier
    {
        public BindingList<DTOSupplier> GetAll()
        {
            return new DAOSupplier().GetAll();
        }

        public void AddNewSupplier(DTOSupplier cty)
        {
            DAOSupplier admin = new DAOSupplier();
            admin.AddNewSupplier(cty);
        }

        public void DeleteSelectedSupplier(DTOSupplier cty)
        {
            DAOSupplier admin = new DAOSupplier();
            admin.DeleteSelectedSupplier(cty);
        }

        public void DeleteAllSupplier()
        {
            DAOSupplier admin = new DAOSupplier();
            admin.DeleteAllSupplier();
        }

        public void UpdateSupplier(DTOSupplier ctyCu, DTOSupplier ctyMoi)
        {
            DAOSupplier admin = new DAOSupplier();
            admin.UpdateSupplier(ctyCu, ctyMoi);
        }
    }
}
