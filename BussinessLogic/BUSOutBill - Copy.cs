using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DTO;
using DAO;

namespace BussinessLogic
{
    public class BUSOutProduct
    {
        public BindingList<DTOOutProduct> GetAll()
        {
            return new DAOOutProduct().GetAll();
        }

        public void AddNewOutProduct(DTOOutProduct hangxuat)
        {
            DAOOutProduct user = new DAOOutProduct();
            user.AddNewOutProduct(hangxuat);
        }

        public void DeleteSelectedOutProduct(DTOOutProduct hangxuat)
        {
            DAOOutProduct user = new DAOOutProduct();
            user.DeleteSelectedOutProduct(hangxuat);
        }

        public void DeleteAllOutProduct()
        {
            DAOOutProduct user = new DAOOutProduct();
            user.DeleteAllOutProduct();
        }

        public void UpdateOutProduct(DTOOutProduct hangxuatCu, DTOOutProduct hangxuatMoi)
        {
            DAOOutProduct user = new DAOOutProduct();
            user.UpdateProduct(hangxuatCu, hangxuatMoi);
        }
    }
}

