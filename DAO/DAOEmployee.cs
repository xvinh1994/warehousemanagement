﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;

namespace DAO
{
    public class DAOEmployee
    {
        string connectionStr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Path.GetFullPath("QLKho.mdf") + ";Integrated Security=True";

        public BindingList<DTOEmployee> GetAll()
        {
            BindingList<DTOEmployee> result = new BindingList<DTOEmployee>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from Employee";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOEmployee nvien = new DTOEmployee();
                nvien.EID = reader["eid"].ToString();
                nvien.Name = reader["name"].ToString();
                nvien.Supplier_id = reader["supplier_id"].ToString();
                nvien.Username = reader["username"].ToString();
                nvien.Password = reader["password"].ToString();
                nvien.IsAdmin = Convert.ToBoolean(reader["isadmin"].ToString());

                result.Add(nvien);
            }

            conn.Close();

            return result;
        }

        public void AddNewUser(DTOEmployee nvien)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "insert into Employee(eid, supplier_id, name, username, password, isadmin) values(@eid, @sid, @name, @username, @password, @isadmin)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@eid", nvien.EID);
            cmd.Parameters.AddWithValue("@sid", nvien.Supplier_id);
            cmd.Parameters.AddWithValue("@name", nvien.Name);
            cmd.Parameters.AddWithValue("@username", nvien.Username);
            cmd.Parameters.AddWithValue("@password", nvien.Password);
            cmd.Parameters.AddWithValue("@isadmin", nvien.IsAdmin.ToString());
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteUser(DTOEmployee nvien)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr; 
            conn.Open();

            string query = "delete from Employee where eid='" + nvien.EID + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteAllUser()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Employee";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void UpdateUser(DTOEmployee nvienCu, DTOEmployee nvienMoi)
        {
            DeleteUser(nvienCu);
            AddNewUser(nvienMoi);
        }
    }
}
