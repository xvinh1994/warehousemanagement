﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;

namespace DAO
{
    public class DAOInProduct
    {
        string connectionStr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Path.GetFullPath("QLKho.mdf") + ";Integrated Security=True";

        public BindingList<DTOInProduct> GetAll()
        {
            BindingList<DTOInProduct> result = new BindingList<DTOInProduct>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from In_Product";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOInProduct hangnhap = new DTOInProduct();
                hangnhap.PID = reader["pid"].ToString();
                hangnhap.IBID = reader["ibid"].ToString();
                hangnhap.Quantity = int.Parse(reader["quantity"].ToString());

                result.Add(hangnhap);
            }

            conn.Close();

            return result;
        }

        public BindingList<DTOInProduct> GetAllFromSupplier(DTOEmployee nvien)
        {
            BindingList<DTOInProduct> result = new BindingList<DTOInProduct>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from In_Product as inProd, Product as p, Employee as e where e.supplier_id='" + nvien.Supplier_id + "' and p.pid=inProd.pid";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOInProduct hangnhap = new DTOInProduct();
                hangnhap.PID = reader["pid"].ToString();
                hangnhap.IBID = reader["ibid"].ToString();
                hangnhap.Quantity = int.Parse(reader["quantity"].ToString());

                result.Add(hangnhap);
            }

            conn.Close();

            return result;
        }

        public void AddNewInProduct(DTOInProduct hangnhap)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "insert into In_Product(pid, ibid, quantity) values(@pid, @ibid, @quantity)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@pid", hangnhap.PID);
            cmd.Parameters.AddWithValue("@ibid", hangnhap.IBID);
            cmd.Parameters.AddWithValue("@quantity", hangnhap.Quantity);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteSelectedInProduct(DTOInProduct hangnhap)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from In_Product where pid='" + hangnhap.PID + "' and ibid='" + hangnhap.IBID + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteAllInProduct()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from In_Product";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void UpdateInProduct(DTOInProduct hangNhapCu, DTOInProduct hangNhapMoi)
        {
            DeleteSelectedInProduct(hangNhapCu);
            AddNewInProduct(hangNhapMoi);
        }
    }
}
