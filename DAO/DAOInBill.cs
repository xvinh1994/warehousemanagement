﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;

namespace DAO
{
    public class DAOInBill
    {
        string connectionStr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Path.GetFullPath("QLKho.mdf") + ";Integrated Security=True";

        public BindingList<DTOInBill> GetAll()
        {
            BindingList<DTOInBill> result = new BindingList<DTOInBill>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from In_bill";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOInBill donnhap = new DTOInBill();
                donnhap.EID = reader["eid"].ToString();
                DateTime tmp;
                if (DateTime.TryParse(reader["date"].ToString(), out tmp))
                    donnhap.Date = tmp;
                else donnhap.Date = DateTime.Now;
                donnhap.Supplier_id = reader["supplier_id"].ToString();
                donnhap.IBID = reader["ibid"].ToString();
                donnhap.Reason = reader["reason"].ToString();

                result.Add(donnhap);
            }

            conn.Close();

            return result;
        }

        public void AddNewInBill(DTOInBill hoadonnhap)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "insert into In_Bill(ibid, date, reason, eid, supplier_id) values(@ibid, @date, @reason, @eid, @supplier_id)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@eid", hoadonnhap.EID);
            cmd.Parameters.AddWithValue("@supplier_id", hoadonnhap.Supplier_id);
            cmd.Parameters.AddWithValue("@ibid", hoadonnhap.IBID);
            cmd.Parameters.AddWithValue("@reason", hoadonnhap.Reason);
            cmd.Parameters.AddWithValue("@date", hoadonnhap.Date);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteSelectedInBill(DTOInBill hoadonnhap)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from In_Bill where ibid='" + hoadonnhap.IBID + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteAllInBill()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from In_Bill";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void UpdateInBill(DTOInBill hoadonCu, DTOInBill hoadonMoi)
        {
            DeleteSelectedInBill(hoadonCu);
            AddNewInBill(hoadonMoi);
        }
    }
}
