﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;

namespace DAO
{
    public class DAOOutProduct
    {
        string connectionStr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Path.GetFullPath("QLKho.mdf") + ";Integrated Security=True";

        public BindingList<DTOOutProduct> GetAll()
        {
            BindingList<DTOOutProduct> result = new BindingList<DTOOutProduct>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from Out_Product";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOOutProduct hangxuat = new DTOOutProduct();
                hangxuat.PID = reader["pid"].ToString();
                hangxuat.OBID = reader["obid"].ToString();
                hangxuat.Quantity = int.Parse(reader["quantity"].ToString());

                result.Add(hangxuat);
            }

            conn.Close();

            return result;
        }

        public void AddNewOutProduct(DTOOutProduct hangxuat)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "insert into out_Product(pid, obid, quantity) values(@pid, @obid, @quantity)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@pid", hangxuat.PID);
            cmd.Parameters.AddWithValue("@obid", hangxuat.OBID);
            cmd.Parameters.AddWithValue("@quantity", hangxuat.Quantity);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteSelectedOutProduct(DTOOutProduct hangxuat)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Out_Product where obid='" + hangxuat.OBID + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteAllOutProduct()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Out_Product";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void UpdateOutProduct(DTOOutProduct hangxuatCu, DTOOutProduct hangxuatMoi)
        {
            DeleteSelectedOutProduct(hangxuatCu);
            AddNewOutProduct(hangxuatMoi);
        }
    }
}
