﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;

namespace DAO
{
    public class DAOProduct
    {
        string connectionStr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Path.GetFullPath("QLKho.mdf") + ";Integrated Security=True";

        public BindingList<DTOProduct> GetAll()
        {
            BindingList<DTOProduct> result = new BindingList<DTOProduct>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from Product";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOProduct sp = new DTOProduct();
                sp.PID = reader["pid"].ToString();
                sp.Name = reader["name"].ToString();
                sp.Supplier_id = reader["sid"].ToString();
                sp.Description = reader["description"].ToString();
                sp.InPrice = float.Parse(reader["in_price"].ToString());
                sp.OutPrice = float.Parse(reader["out_price"].ToString());
                sp.Quantity = int.Parse(reader["quantity"].ToString());
                sp.MinQuantity = int.Parse(reader["min_quantity"].ToString());
                sp.Note = reader["note"].ToString();

                result.Add(sp);
            }

            conn.Close();

            return result;
        }

        public BindingList<DTOProduct> GetProductFromSupplier(DTOSupplier cty)
        {
            BindingList<DTOProduct> result = new BindingList<DTOProduct>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from Product where supplier_id='" + cty.Supplier_id + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOProduct sp = new DTOProduct();
                sp.PID = reader["pid"].ToString();
                sp.Name = reader["name"].ToString();
                sp.Supplier_id = reader["supplier_id"].ToString();
                sp.Description = reader["description"].ToString();
                sp.InPrice = float.Parse(reader["in_price"].ToString());
                sp.OutPrice = float.Parse(reader["out_price"].ToString());
                sp.Quantity = int.Parse(reader["quantity"].ToString());
                sp.MinQuantity = int.Parse(reader["min_quantity"].ToString());
                sp.Note = reader["note"].ToString();

                result.Add(sp);
            }

            conn.Close();

            return result;
        }

        public void AddNewProduct(DTOProduct sp)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "insert into Product(pid, sid, name, description, in_price, out_price, quantity, min_quantity, note) values(@pid, @supplier_id, @name, @description, @in_price, @out_price, @quantity, @min_quantity, @note)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@pid", sp.PID);
            cmd.Parameters.AddWithValue("@supplier_id", sp.Supplier_id);
            cmd.Parameters.AddWithValue("@name", sp.Name);
            cmd.Parameters.AddWithValue("@description", sp.Description);
            cmd.Parameters.AddWithValue("@in_price", sp.InPrice);
            cmd.Parameters.AddWithValue("@out_price", sp.OutPrice);
            cmd.Parameters.AddWithValue("@quantity", sp.Quantity);
            cmd.Parameters.AddWithValue("@min_quantity", sp.MinQuantity);
            cmd.Parameters.AddWithValue("@note", sp.Note);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteProduct(DTOProduct sp)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Product where pid='" + sp.PID + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteProductFromSupplier(DTOSupplier cty)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Product where sid='" + cty.Supplier_id + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteAllProduct()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Product";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void UpdateProduct(DTOProduct spCu, DTOProduct spMoi)
        {
            DeleteProduct(spCu);
            AddNewProduct(spMoi);
        }
    }
}
