﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;

namespace DAO
{
    public class DAOSupplier
    {
        string connectionStr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Path.GetFullPath("QLKho.mdf") + ";Integrated Security=True";

        public BindingList<DTOSupplier> GetAll()
        {
            BindingList<DTOSupplier> result = new BindingList<DTOSupplier>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from Supplier";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOSupplier cty = new DTOSupplier();
                cty.Supplier_id = reader["sid"].ToString();
                cty.Name = reader["name"].ToString();
                cty.PhoneNumber = reader["phone_number"].ToString();
                cty.Address = reader["Address"].ToString();

                result.Add(cty);
            }

            conn.Close();

            return result;
        }

        public void AddNewSupplier(DTOSupplier cty)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "insert into Supplier(supplier_id, name, address, phone_number) values(@supplier_id, @name, @address, @phone_number)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@supplier_id", cty.Supplier_id);
            cmd.Parameters.AddWithValue("@name", cty.Name);
            cmd.Parameters.AddWithValue("@address", cty.Address);
            cmd.Parameters.AddWithValue("@phone_number", cty.PhoneNumber);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteSelectedSupplier(DTOSupplier cty)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Supplier where supplier_id='" + cty.Supplier_id + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteAllSupplier()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Supplier";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void UpdateSupplier(DTOSupplier ctyCu, DTOSupplier ctyMoi)
        {
            DeleteSelectedSupplier(ctyCu);
            AddNewSupplier(ctyMoi);
        }
    }
}
