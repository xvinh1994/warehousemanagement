﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;

namespace DAO
{
    public class DAOOutBill
    {
        string connectionStr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Path.GetFullPath("QLKho.mdf") + ";Integrated Security=True";

        public BindingList<DTOOutBill> GetAll()
        {
            BindingList<DTOOutBill> result = new BindingList<DTOOutBill>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "select * from Out_Bill";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DTOOutBill hoadon = new DTOOutBill();
                hoadon.EID = reader["eid"].ToString();
                DateTime tmp;
                if (DateTime.TryParse(reader["date"].ToString(), out tmp))
                    hoadon.Date = tmp;
                else hoadon.Date = DateTime.Now;
                hoadon.Supplier_id = reader["supplier_id"].ToString();
                hoadon.Reason = reader["reason"].ToString();
                hoadon.OBID = reader["obid"].ToString();

                result.Add(hoadon);
            }

            conn.Close();

            return result;
        }

        public void AddNewOutBill(DTOOutBill hoadon)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "insert into OutBill(obid, date, reason, eid, supplier_id) values(@obid, @date, @reason, @eid, @supplier_id)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@eid", hoadon.EID);
            cmd.Parameters.AddWithValue("@supplier_id", hoadon.Supplier_id);
            cmd.Parameters.AddWithValue("@obid", hoadon.OBID);
            cmd.Parameters.AddWithValue("@reason", hoadon.Reason);
            cmd.Parameters.AddWithValue("@date", hoadon.Date);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteSelectedOutBill(DTOOutBill hoadon)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Out_Bill where obid='" + hoadon.OBID + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void DeleteAllOutBill()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connectionStr;
            conn.Open();

            string query = "delete from Out_Bill";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void UpdateOutBill(DTOOutBill hoadonCu, DTOOutBill hoadonMoi)
        {
            DeleteSelectedOutBill(hoadonCu);
            AddNewOutBill(hoadonMoi);
        }
    }
}
