﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTOOutBill
    {
        public string OBID { get; set; }
        public DateTime Date { get; set; }
        public string Reason { get; set; }
        public string EID { get; set; }
        public string Supplier_id { get; set; }
    }
}
