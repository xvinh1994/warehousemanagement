﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTOOutProduct
    {
        public string PID { get; set; }
        public string OBID { get; set; }
        public int Quantity { get; set; }
    }
}
