﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTOInProduct
    {
        public string PID { get; set; }
        public string IBID { get; set; }
        public int Quantity { get; set; }
    }
}
