﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTOProduct
    {
        public string PID { get; set; }
        public string Supplier_id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float InPrice { get; set; }
        public float OutPrice { get; set; }
        public int Quantity { get; set; }
        public int MinQuantity { get; set; }
        public string Note { get; set; }
    }
}
